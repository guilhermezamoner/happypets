

## Tecnologias
* Node.js
* Express
* Express Messages, Session, Connect Flash & Validation
* MongoDB & Mongoose
* Pug Templating
* Passport.js Authentication
* BCrypt Hashing

### Version
2.0.0

## Usage

### Installation

Install the dependencies

```sh
$ npm install
```

start mongo Shell 

```sh
$ sudo service mongod start
```
```sh
$ mongo
```
Run app

```sh
$ npm start
```
